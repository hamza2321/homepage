package com.example.homepage;

public class modelClass {
    private int image;
    private String title;

    public modelClass(int image, String title) {
        this.image = image;
        this.title = title;
    }

    public int getImage() {
        return image;
    }

    public String getTitle() {
        return title;
    }
}
