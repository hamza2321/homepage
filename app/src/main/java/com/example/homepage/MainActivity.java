package com.example.homepage;



import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    TextView featured;
    ViewPager viewPager;
    PagerViewAdapter pagerViewAdapter;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

        viewPager = (ViewPager) findViewById(R.id.viewPager_slider);

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(this);

        viewPager.setAdapter(viewPagerAdapter);


        //yha sy

        featured.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {

                viewPager.setCurrentItem(0);

            }
        });



        featured = (TextView)findViewById(R.id.featured);
        viewPager = (ViewPager)findViewById(R.id.viewPager_comic);

        pagerViewAdapter = new PagerViewAdapter(getSupportFragmentManager());

        viewPager.setAdapter(pagerViewAdapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                onChangeTab(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });



        //yha sy

        ArrayList<modelClass> items = new ArrayList<>();
        CustomAdapter adapter = new CustomAdapter(this, items);

        RecyclerView recyclerView = findViewById(R.id.recycler_view_sheeba);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        recyclerView.setAdapter(adapter);

// let's create 10 random items

        for (int i = 0; i < 10; i++) {
            items.add(new modelClass(R.drawable.slide3, "Title " + i));
            adapter.notifyDataSetChanged();
        }



    }

    private void onChangeTab(int position) {
        if (position == 0){
            featured.setTextSize(25);
        }
    }

}